\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cubedp}[2022/01/10 AcubeSAT Data Package Class modified for FakeSat]

% Template version
\def\ACdptemplateversion{vd1-fs}

% Class options
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{cubedoc}}

% Options
\makeatletter
\newif\ifdiff\difffalse
\DeclareOption{diff}{\difftrue}
\makeatother
\ProcessOptions\relax

% Load the `cals` package before cubedoc, to allow renaming the commands
\RequirePackage{cals}
% Rename the cals commands to prevent conflict with the `tablestyles` package
\let\calsthead\thead
\let\thead=\relax
\let\calstfoot\tfoot
\let\tfoot=\relax

\LoadClass[bibliography]{cubedoc}

\docidreffalse % Don't show the final

%%%
%%% PACKAGES
%%%
% Provides the {landscape} environment for wide pages
\RequirePackage{pdflscape}
\RequirePackage{multicol}
\usepackage[nameinlink,capitalise]{cleveref}
\RequirePackage{tcolorbox}
\tcbuselibrary{skins,xparse,breakable}

% String manipulation functions
\RequirePackage{xstring}

% Columns for procedures table
\newcolumntype{P}[1]{>{\centering\arraybackslash}m{#1-2\tabcolsep}}

%%%
%%% COLOURS
%%%
\colorlet{tablegray}{gray!50!white}

%%%
%%% DATETIME
%%%
\iffinal
	% Load TeX OS query that allows getting information about the timezone
	\usepackage{texosquery}
\fi
\usepackage[style=iso,showseconds=false,calc]{datetime2}

%%%
%%% TITLE
%%%
\makeatletter
% Title that contains typographic elements
\newcommand{\thettitle}{\thetitle}
\newcommand{\therawtitle}{\thedtitle}
\RenewDocumentCommand{\title}{om}{%
	% #1: title abbreviation (optional), #2: full title
	\IfNoValueTF{#1}{
		\ac@old@title{#2}
		\renewcommand{\thedtitle}{#2}
	}{
		\StrSubstitute{#1}{\_}{_}[\ac@nickname@raw]
		\edef\ac@nickname{\detokenize\expandafter\expandafter\expandafter{\expandafter\ac@nickname@raw}}
		\ac@old@title{#2 [\ac@nickname]}
		\renewcommand{\thedtitle}{#2 {[\ac@nickname]}}
		\renewcommand{\thettitle}{#2\\[0.8ex]{\texttt{[\ac@nickname]}}}
		\renewcommand{\theheadertitle}{#2~\\{[\ac@nickname]}}
		\renewcommand{\therawtitle}{#2}
		\newcommand{\thenickname}{\ac@nickname}
	}
}
\makeatother

%%%
%%% CALS tables, used as a workaround for border bugs in PDF rendering
%%%
\makeatletter
\newenvironment{calinfotable}{%
	\begin{calstable}[c]
		\def\cals@framers@width{0.4pt}   % Outside frame rules, reduce if the rule is too heavy
		\def\cals@framecs@width{0.4pt}
		\def\cals@bodyrs@width{0.4pt}
		\cals@setpadding{Ag}
		\cals@setcellprevdepth{Al}
		\def\cals@cs@width{0.4pt}             % Inside rules, reduce if the rule is too heavy
		\def\cals@rs@width{0.4pt}
		\def\cals@bgcolor{}
		% "Switch" to turn colour on and off
		\def\calstogglegray{\ifx\cals@bgcolor\empty
				\def\cals@bgcolor{gray!50}
			\else \def\cals@bgcolor{} \fi}
		\makeatother
		%\newcommand\bbrow{\brow\setbox\cals@current@row=\hbox{\vbox to 2em{}}}
		\setlength{\cals@paddingT}{5pt}
		\setlength{\cals@paddingB}{3pt}
		}{\end{calstable}}
\makeatother

%%%
%%% SI UNITS, for those who want to use them
%%%
\usepackage[binary-units=true]{siunitx}
\sisetup{detect-all} % When using \SI in text, use a non-math font
\sisetup{load-configurations = abbreviations}


%%%
%%% Confidential information
%%%

\tcbset{notice/.style={
		enhanced,
		breakable,
		parbox=false,
		colbacktitle=gray!70!black!85!spacedot,
		colframe=black,
		frame hidden,
		bottomtitle=1ex,
		toptitle=1ex,
		boxrule=0mm,
		rightrule=0pt, %reserve space
		borderline west={0.8pt}{0pt}{white!50!black},%---- draw line
		interior hidden,
		sharp corners=all,
		enlarge left by= -5mm,
		enlarge right by= -5mm,
		width=\linewidth + 10mm,
		center title
	}
}

\ifconfidential
\DeclareTColorBox{copyright}{m}{
	notice,
	title={Copyright #1}%
}
\else
    \RenewEnviron{copyright}[1]{}{}
\fi

\ifconfidential
\DeclareTColorBox{proprietary}{m}{
	notice,
	title={Proprietary Information belonging to #1}%
}
\else
    \NewEnviron{proprietary}[1]{}{}
\fi

% Command for writing draft notes in non-final document versions
\newcommand{\draft}[1]{%
  \iffinal%
    \ClassError{cubedp.cls}{Draft commands in final document}{Did you perhaps forget to remove any leftover draft commands?}%
  \else%
  	\ClassWarning{cubedp.cls}{Draft command remaining}%
    {\leavevmode\color{red!50!black}#1}%
  \fi%
}

% Commands to mark differences between versions
\RequirePackage{float}
\RequirePackage[xcolor]{changebar}
\RequirePackage{xspace}
\colorlet{diff}{green!35!black} % Value chosen to pass WCAG AA

\providetoggle{diffactivated}
\settoggle{diffactivated}{false}

\newcommand{\ditoc}[1]{%
	\ifdiff%
		{\leavevmode\textcolor{diff}{#1}}%
	\else%
		{\leavevmode#1}%
	\fi%
}

\newcommand{\di}[1]{%
	\ifdiff%
	\iftoggle{diffactivated}{}{\cbstart}%
		{\leavevmode\color{diff}#1}%
	\iftoggle{diffactivated}{}{\cbend}%
	\else%
		{\leavevmode#1}%
	\fi%
	\xspace%
}

\cbcolor{diff!50!white}
\setlength\changebarwidth{1.2pt}
\setlength\changebarsep{5\marginparsep}
\NewDocumentEnvironment{diff}{o}
{%
    \IfNoValueF{#1}{\setlength\changebarsep{#1}}
	\ifdiff%
	    \settoggle{diffactivated}{true}%
		\cbstart%
	\fi%
}{%
	\ifdiff%
		\cbend%
		\settoggle{diffactivated}{false}%
	\fi%
}

%%%
%%% AUTHORS
%%%
\makeatletter
\def\ac@authortable{}%
\RenewDocumentCommand{\author}{oom}{%
	% #1: Role in Document
	% #2: Role in Project
	% #3: Full Name (First Name - Last Name)
	\appto\ac@authortext{#3%
		\IfNoValueF{#1}{ (#1)}
		\\}
	\iffinal
		% For authors and not other participants...
		\IfSubStr{#1}{Author}{%
			% Append to the authortext variable that contains commas instead of newlines
			\ifdefempty{\ac@authortext@comma}{%
				\appto\ac@authortext@comma{#3}% Do not add a comma if this is the first element
			}{%
				\appto\ac@authortext@comma{, #3}%
			}%
		}%
	\fi
	\appto\ac@authortable{
		\brow
		\calstogglegray\bfseries\alignL
		\cell{\sloppy\vfil\vphantom{ly}#1\par}
		\calstogglegray\mdseries
		\IfNoValueTF{#2}{%
			\nullcell{ltb}\nullcell{rtb}
			\spancontent{\vfil\vphantom{ly}#3}
		}{%
			\alignL\cell{\sloppy\vfil\vphantom{ly}#2\par}
			\alignL\cell{\vfil\vphantom{ly}#3}
		}
		\erow
	}%
}
\makeatother

%%%
%%% Applicable Documents
%%%

\newbibmacro{title+url}[1]{{\href{\thefield{url}}{#1}}}
\newbibmacro{versionlabel}[1]{%
	\IfSubStr{#1}{Rev}{}{%
		\IfSubStr{#1}{Corr}{}{%
			\bibstring{version}~%
		}%
	}%
}

% Use title as anchor text for the URL (if it exists)
\DeclareFieldFormat{title}{\usebibmacro{title+url}{\mkbibemph{#1}}}
\DeclareFieldFormat{url}{}%#1}
\DeclareFieldFormat{number}{\texttt{#1}}
\DeclareFieldFormat{version}{\usebibmacro{versionlabel}{#1}#1}

\DeclareBibliographyCategory{applicabledocs}

% All documents of the @applicabledoc type are added into the
% applicabledocs category
\AtDataInput{
	\ifentrytype{applicabledoc}
	{\addtocategory{applicabledocs}{\thefield{entrykey}}}{
}}

% Redefine cite in order to print different citation labels depending on the entry type
\DeclareCiteCommand{\cite}[]
  {\usebibmacro{prenote}}
  {%
         % If reference is of standard type, print its id
        \ifentrytype{standard}{\printtext[bibhyperref]{\texttt{\printfield[citetitle]{number}}}}
        {% Otherwise print its reference number
            \usebibmacro{citeindex}%
            \printtext{[\usebibmacro{cite}]}%
        }%
  }%
  {\multicitedelim}{\usebibmacro{postnote}}%

% Macro to allow setting the category of a file upon citing it.
\makeatletter
\def\citecategory{\@ifnextchar[{\cit@withcategory}{\cit@nocategory}}
\def\cit@withcategory[#1]#2{\addtocategory{#1}{#2}\cite{#2}}
\def\cit@nocategory#1{\cite{#1}}
\makeatother

% All documents of the applicabledocs category should have an 'AD' prefix
\assignrefcontextcats[labelprefix=AD]{applicabledocs}

\newbibmacro{tabular:labelid}{%
  \strfield{number}}

\renewbibmacro{tabular:labeltitle}{%
  \href{\thefield{url}}{\thefield{title}}}

\newbibmacro{tabular:labelversion}{%
  \thefield{version}}

\newbibmacro{tabular:labeldate}{%
  \mkdaterangeiso{}}

\newbibmacro{tabular:labelnumber}{%
    \thefield{labelprefix}%
    \thefield{labelnumber}}

\defbibtabular{bibtabular}{
    \renewcommand*{\arraystretch}{1.3}%
    \begin{adjustbox}{center}
    \begin{tabular}
    {|x{.1\textwidth\relax}|x{.3\textwidth\relax}|x{.42\textwidth\relax}|x{.12\textwidth\relax}|x{.18\textwidth\relax}|}
    \hline
    \rowcolor{lightgray}
    \textbf{\#} & \textbf{Document Number} & \textbf{Document Title} & \textbf{Version} & \textbf{Date}\\
    \hline}
    {\end{tabular}\end{adjustbox}}
    {\anchorlang{\usebibmacro{tabular:labelnumber}}  &
     \texthtt{\anchorlang{\usebibmacro{tabular:labelid}}} &
     \anchorlang{\usebibmacro{tabular:labeltitle}} &
     \anchorlang{\usebibmacro{tabular:labelversion}}&
     \anchorlang{\usebibmacro{tabular:labeldate}} \\ \hline}

% Define a bibliography heading style that displays the title as a \section (large font),
% but adds the TOC listing as a \subsection (inset)
\defbibheading{custombibtoc}[\refname]{%
	\section*{#1}%
	\addcontentsline{toc}{subsection}{#1}%
}

%%%
%%% CHANGELOG
%%%
% Initialise changelog commands to prevent errors when the user hasn't added
% entries to the changelog
\newcommand{\latestRevDate}{\today}
\newcommand{\latestRevStatus}{DRAFT}
\ExplSyntaxOn
% Include new RELEASED status in the list of acceptable statuses
\seq_put_right:Nn \l_acubesat_changelog_statuses_seq {RELEASED}
\RenewDocumentCommand{\changelog}{mmmmO{FakeSat~Team}}{%
% Prepend the version to the changelog table
\preto\thechangelog{\hline#1 & \parbox[t]{2cm}{%
\centering{\vspace{-8pt}\large\texttt{\textbf{#2}}}\\[.3ex]{\scriptsize#3}\vspace{0.7em}%
} & #4\\}%
% Set the latest revision variables
\renewcommand{\latestRevDate}{#1}
\renewcommand{\latestRev}{#2}
\renewcommand{\latestRevStatus}{#3}
% Show an error if the version is invalid
\seq_if_in:NnTF \l_acubesat_changelog_statuses_seq {#3} {} {\ClassError{cubedoc}{Invalid~changelog~Document~Status.~Provide~one~of:~
		DRAFT,~INTERNALLY~RELEASED,~RELEASED,~PUBLISHED,~REVOKED
	}{}}
% Add the version to the XML list
\iffinal
	\prop_clear:N \l_tmpa_prop % Empty the temporary prop list to use it
	% Add the properties to the property list
	\prop_put:Nnn \l_tmpa_prop {modifyDate} {#1}
	\prop_put:Nnn \l_tmpa_prop {status} {#3}
	\prop_put:Nnn \l_tmpa_prop {version} {#2}
	\prop_put:Nnn \l_tmpa_prop {modifier} {#5}
	% Prepend the property list to the sequence of versions
	\seq_put_right:NV \l_acubesat_changelog_xml_seq {\l_tmpa_prop}
\fi
}
\ExplSyntaxOff

%%%
%%% ACRONYMS
%%%
% Acronym list style
\newlist{acronyms}{description}{1}
\setlist[acronyms]{labelwidth = 4.5em,leftmargin = 5em,itemsep=0em,itemindent = 0pt}
\DeclareAcroListStyle{dlist}{list}{ list = acronyms }
\acsetup{ list-style = dlist }

% Disable the "There is no short form set for acronym" warning
\ExplSyntaxOn
\msg_redirect_name:nnn { acro } { substitute-short } { none }
\ExplSyntaxOff
\makeatother

% Count citations
\newtotcounter{bib@citenum}
\AtEveryCitekey{\stepcounter{bib@citenum}}

%%%
%%% DATA PACKAGE ID COMMAND
%%%
\newcommand{\thedpid}{}
\renewcommand{\dpid}[1]{
	% Store the doc id string
	% \detokenize will make sure that _underscores_ are parsed as text
	\renewcommand{\thedpid}{\detokenize{#1}\_\latestRevDate\_v\latestRev}%
}

%%%
%%% DOCUMENT VARIANTS
%%%
\makeatletter
\ExplSyntaxOn
\prop_new:N \l_acubesat_variants_prop
% See the document options to determine variants
\ifpublic
	\prop_put:Nnn \l_acubesat_variants_prop {public} \c_true_bool
\fi
\ifconfidential
	\prop_put:Nnn \l_acubesat_variants_prop {confidential} \c_true_bool
\fi
\ifweb
	\prop_put:Nnn \l_acubesat_variants_prop {web} \c_true_bool
\fi
\ifdiff
	\prop_put:Nnn \l_acubesat_variants_prop {diff} \c_true_bool
\fi
% Create the list of document variants
\seq_new:N \g_acubesat_variants_seq
% For each possible variant, find if it exists...
\prop_get:NnNTF \l_acubesat_variants_prop {web} \l_tmpa_tl {
	\seq_put_right:Nn \g_acubesat_variants_seq {Web}
}{
	\seq_put_right:Nn \g_acubesat_variants_seq {Print}
}
\prop_get:NnNT \l_acubesat_variants_prop {public} \l_tmpa_tl {
	\seq_put_right:Nn \g_acubesat_variants_seq {Public}
}
\prop_get:NnNT \l_acubesat_variants_prop {confidential} \l_tmpa_tl {
	\seq_put_right:Nn \g_acubesat_variants_seq {Confidential}
}
\prop_get:NnNT \l_acubesat_variants_prop {diff} \l_tmpa_tl {
	\seq_put_right:Nn \g_acubesat_variants_seq {Diff}
}
% Command to display a comma-separated list of document variants
\newcommand{\ac@variants}{
	% Output comma-separated values
	\str_clear:N \l_tmpa_str

	% Append the comma-separated list of requirements into the string
	\seq_map_inline:Nn \g_acubesat_variants_seq {
		\str_put_right:Nn \l_tmpa_str {##1,~}
	}

	% Remove the last comma
	\str_range:Nnn \l_tmpa_str {1} {-3}
}
\ExplSyntaxOff
\makeatother

%%%
%%% DISTRIBUTION LIST
%%%
\makeatletter
\ExplSyntaxOn
% Create a global list of distribution strings
\seq_new:N \g_acubesat_distribution_seq
% Define the contents of the distribution table itself
\def\ac@distributiontable{}
\RenewDocumentCommand{\distribution}{m}{%
	\appto\ac@distributiontable{%
		#1%
		\\\hline%
	}%
	\iffinal
		\seq_put_right:Nn \g_acubesat_distribution_seq {#1}
	\fi
}
\ExplSyntaxOff
\makeatother

%%%
%%% INTRODUCTION
%%%
\RequirePackage{environ}
\makeatletter
% Create a command to expand the provided argument (LaTeX shenanigans)
\newrobustcmd{\ac@setintroduction}[1]{%
	\gdef\ac@introduction{#1}%
}
\NewEnviron{introduction}{%
	% Force expansion of the body, then store it into the new variable so it can be parsed below
	\expandafter\ac@setintroduction\expandafter{\BODY}
}
\makeatother

%%%
%%% TITLE PAGE
%%%
\makeatletter
\RequirePackage[absolute]{textpos}
\def\@maketitle{%
	\newpage%
	% Don't display fancy headers
	\thispagestyle{plain}
	% Place TOC link at top left of page
	\begin{textblock}{0}(0,0)%
		\phantomsection%
		\addcontentsline{toc}{section}{Preamble}%
	\end{textblock}%
	% Define a new length for the separation of the cover
	\newlength{\coversep}%
	\setlength{\coversep}{\dimexpr(1.18in+\voffset+\topmargin+\headheight+\headsep+\parskip)\relax}%
	%
	% Adjustbox makes a box that takes up the full width of the page
	\begin{adjustbox}{center,vspace*={{-\coversep} {0in}}}
		% The second \parbox parameter specifies the height of the logo
		\colorbox{ACheadercolor}{\parbox[b][9cm][c]{\paperwidth}{\begin{center}
					\includegraphics[align=c,height=8cm]{media/logo}%
		\end{center}}}
	\end{adjustbox}
	
	\setlength{\parskip}{0pt}%
	\null%
	\vskip 1em plus 0.5em minus 0.5em%
	\begin{center}%
		\let \footnote \thanks%
		{\Huge\textbf\thettitle}%
	\end{center}%
	\vskip 3em%
	\begin{center}%
		{\LARGE\texttt{\thedpid}}\\%
		{\small Document reference}
	\end{center}%
	\par
	\vskip 3em%
	\begin{center}%
		{\LARGE Version: \texttt{\latestRev}}\\[2ex]%
		{\LARGE\latestRevDate}%
		\unless\iffinal%
			\\[1ex]{\color{error}\LARGE \textbf{Draft version}}
		\fi%
	\end{center}%
	\vskip 1em plus 1filll minus 0pt%
	% Adjustbox makes a box that takes up the full width of the page
	\vskip 2em plus 1filll minus 0pt%
	\begin{center}
		\begin{tabularx}{\linewidth}{|X|}
			\unless\ifpublic
				\hline
				\rowcolor{tablegray} \textbf{Distribution List} \\ \hline
				\ac@distributiontable
			\else
				\hline
				\rowcolor{tablegray} \textbf{Disclaimer} \\ \hline
				This is a public version of the document.
				\\ \hline
			\fi
		\end{tabularx}
	\end{center}
	\clearpage\newpage%
}
\makeatother

%%%
%%% PREAMBLE
%%%
\RequirePackage{longtable}
\makeatletter
\def\ac@preamble{
	% Hypersetup has to be executed here, so that all other variables are ready
	\hypersetup{%
		pdfauthor   = {FakeSat Team},
		pdfkeywords = {\thedocid, \thedpid, \thenickname, \AClistcategories},
		pdftitle    = {\@title},
		pdfsubject  = {\therawtitle},
		bookmarksnumbered = true, % Show section numbering in the PDF table of contents. Allows easier browsing of the document
	}

	\maketitle

	% Add headers/footers to these pages (with workarounds for LaTeX not always working)
	\thispagestyle{fancy}
	\pagestyle{fancy}
	\addtocontents{toc}{\protect\thispagestyle{fancy}}

	\section*{Authors}
	\addcontentsline{toc}{subsection}{Authors}
	\begin{calinfotable}
		% Set up column widths
		\colwidths{{\dimexpr(\columnwidth)/100*22\relax} % 22%
				{\dimexpr(\columnwidth)/100*25\relax} % 25%
				{\dimexpr(\columnwidth)/100*53\relax} % 53%
		}

		% Table header
		\calsthead{\bfseries
			\brow
			\calstogglegray\alignL\cell{\vfil\vphantom{ly}Document Role}
			\alignL\cell{\vfil\vphantom{ly}Project Role}
			\alignL\cell{\vfil\vphantom{ly}Full Name}
			\calstogglegray
			\erow
			\mdseries
		}
		\calstfoot{\lastrule\strut}

		% Table content
		\ac@authortable
	\end{calinfotable}

	\section*{Document Information}
	\addcontentsline{toc}{subsection}{Document Information}
	\begin{calinfotable}
		% Set up column widths to 1/4
		\colwidths{
			{\dimexpr(\columnwidth)/4\relax}
				{\dimexpr(\columnwidth)/4\relax}
				{\dimexpr(\columnwidth)/4\relax}
				{\dimexpr(\columnwidth)/4\relax}
		}

		% Macros for quick addition of values in this table
		\newcommand{\longrow}[2]{
			\brow
			\calstogglegray\bfseries\alignL
			\cell{\vfil\vphantom{ly}##1}
			\calstogglegray\mdseries\alignL
			\nullcell{ltb} \nullcell{tb}\nullcell{rtb}
			\spancontent{\sloppy\vfil\vphantom{ly}##2\par}
			\erow
		}
		\newcommand{\shortrowl}[2]{
			\brow
			\calstogglegray\bfseries\alignL
			\cell{\vfil\vphantom{ly}##1}
			\calstogglegray\mdseries\alignL
			\cell{\vfil\vphantom{ly}##2}
		}
		\newcommand{\shortrowr}[2]{
			\calstogglegray\bfseries\alignL
			\cell{\vfil\vphantom{ly}##1}
			\calstogglegray\mdseries\alignL
			\cell{\vfil\vphantom{ly}##2}
			\erow
		}

		% Title row
		\longrow{Title}{\thedtitle}

		\longrow{Project}{FakeSat}

		\longrow{Institution}{Penumbra University}

		\shortrowl{Revision Date}{\latestRevDate}
		\DTMsavenow{now}
		\DTMtozulu{now}{utcnow}
		\shortrowr{Render Date}{\DTMusedate{utcnow}~\DTMusetime{utcnow}}

		\shortrowl{Status}{\iffinal%
				\latestRevStatus%
			\else%
				\textcolor{error}{\textbf{Draft Version}}
			\fi
		}
		\shortrowr{Variant}{{\sloppy\ac@variants\par}}

		\shortrowl{Documentation\\Template version}{\texttt{\ACdoctemplateversion}}
		\shortrowr{Data Package\\Template version}{\texttt{\ACdptemplateversion}}
	\end{calinfotable}

	\section*{Changelog}
	\addcontentsline{toc}{subsection}{Changelog}
	\vspace{-0.5cm}
	\bgroup % Begin group to keep length parameters local
	\setlist[1]{nosep,after=\vspace{-\baselineskip}}
	\appto{\tableitemize}{\vspace{-\baselineskip}}
	\renewcommand{\arraystretch}{1.5}
	% Longtable magic to center the table beyond page margins
	\setlength\LTleft{-1cm plus 1fill minus 1fill}
	\setlength\LTright{-1cm plus 1fill minus 1fill}
	\begin{longtable}{|C{2.1cm}|C{2cm}|p{10.2cm}|}
		\hline
		\rowcolor{gray!50!white}
		\textbf{Date} & \textbf{Version} & \multicolumn{1}{c|}{\textbf{Details}} \\
		\endfirsthead \hline
		\rowcolor{gray!50!white}
		\textbf{Date} & \textbf{Version} & \multicolumn{1}{c|}{\textbf{Details}} \\
		\endhead
		\thechangelog
		\hline
	\end{longtable}
	\egroup

	\ifdef{\afterinit}{\afterinit}{%
		\unless\iffinal
			This is a {\color{error}draft} version (\texttt{\color{error}\latestRev}) of this document, as of \today.
		\fi

		\ifconfidential
			This document contains proprietary data, confidential information, or assets covered by Non-Disclosure Agreements. As such, it should be considered confidential and sharing must be restricted, as specified on the programme's Terms and Conditions.
		\fi

		\begin{minipage}[c]{.74\textwidth}
			% Attempt to justify even the last line of the paragraph
			\setlength{\parindent}{0pt}
			\setlength{\parskip}{.5\baselineskip}
			\setlength{\parfillskip}{0pt}
			\setlength{\emergencystretch}{.5\textwidth}
			This document is licensed under a \href{https://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution 4.0 International License}. You can find a copy of the license at:
			% Stretch the font out enought that it reaches the right edge of the minipage
			\englishfonttt\addfontfeature{LetterSpace=2.2}
			\href{https://creativecommons.org/licenses/by/4.0/legalcode}{https://creativecommons.org/licenses/by/4.0/legalcode}
		\end{minipage}%
		\hfill%
		\begin{minipage}[c]{.22\textwidth}
			\href{https://creativecommons.org/licenses/by/4.0/}{\includegraphics[width=\textwidth]{media/cc-by.eps}}
		\end{minipage}

		\clearpage%
		\newpage%
	}

	\clearpage\newpage
	\renewcommand{\contentsname}{Table Of Contents}%
	\tableofcontents

	\ifthenelse{\theac@acronyms > 0}{%
		\clearpage\newpage
		\section*{List of Abbreviations}
		\addcontentsline{toc}{subsection}{List of Abbreviations}%
	
		% Count the number of acronyms. If it is too high, split them to 2
		\ifthenelse{\theac@acronyms > 35}{%
			\bgroup%
			\setlength{\columnsep}{1cm}%
			\setlength{\columnseprule}{0.1pt}%
			\renewcommand{\columnseprulecolor}{\color{gray!50!white}}%
			\begin{multicols}{2}%
				\printacronyms[heading=none]%
			\end{multicols}%
			\egroup%
		}{%
			\printacronyms[heading=none]%
		}%
	}%

	\ifdef{\ac@introduction}{%
		\pagebreak[4]%
		\section*{Introduction}%
		\addcontentsline{toc}{subsection}{Introduction}%
		\ac@introduction%
	}{}

	\pagebreak[4]

	\begin{refcontext}[labelprefix=AD]
		\printbibtabular[category=applicabledocs,title={Applicable Documents},heading=custombibtoc]
	\end{refcontext}

	\printbibliography[resetnumbers=true,locallabelwidth=true,title={Reference Documents},notcategory=applicabledocs,heading=custombibtoc]%

	\ifdef{\afterpreamble}{\afterpreamble}{%
		\clearpage%
		\newpage%
	}
}
% Workaround issue where starred section headings are not linkable by hyperref when titlesec is loaded
\AtBeginDocument{%
	\def\ttl@useclass#1#2{%
		\@ifstar%
		{\ttl@labelfalse\phantomsection#1{#2}[]}% {\ttl@labelfalse#1{#2}[]}%
		{\ttl@labeltrue\@dblarg{#1{#2}}}}%
}
\makeatother

%%% Testing Procedures table

\NewDocumentEnvironment{procedures}{m m}
  {%
  \begin{tcolorbox}[left=0mm,right=0mm,boxsep=0mm,bottom=0mm,breakable,width=\linewidth,colback={white},title={\LARGE #1},lefttitle=0.4cm,colbacktitle=darkgray,coltitle=white]    
    \begin{tabular}[H]{lc}
\large \textbf{Date:} & \\
\large \textbf{Location:} & \\
\large \textbf{Activity Description:} & #2\\
\large \textbf{Operators:} & \\
\end{tabular}

  \begin{tiny}
  \centering
  \begin{longtable}{P{0.04\linewidth}|P{0.15 \linewidth}|P{0.15\linewidth} | P{0.15\linewidth} | P{0.15\linewidth}| P{0.07\linewidth} | P{0.13\linewidth} | P{0.052\linewidth} | P{0.10\linewidth}
      }
      \hline
       \rowcolor{tablegray}
       \footnotesize \textbf{Step ID} & \footnotesize  \textbf{Instructions} & \footnotesize   \textbf{Expected Results} & \footnotesize \textbf{Actual Results} & \footnotesize \textbf{Pass/Fail Criteria} &  \footnotesize \textbf{Passed} &  \footnotesize \textbf{Remarks} &   \footnotesize \textbf{Date} &  \footnotesize \textbf{PA signature}
       \\ \hline
       \endfirsthead
       \hline
       \rowcolor{tablegray}
       \footnotesize \textbf{Step ID} & \footnotesize  \textbf{Instructions} & \footnotesize   \textbf{Expected Results} & \footnotesize \textbf{Actual Results} & \footnotesize \textbf{Pass/Fail Criteria} &  \footnotesize \textbf{Passed} &  \footnotesize \textbf{Remarks} &   \footnotesize \textbf{Date} &  \footnotesize \textbf{PA signature}
       \\ \hline
       \endhead
  }
  {%
    \end{longtable}%
    \end{tiny}%
    \end{tcolorbox}%
    }
  
\DeclareExpandableDocumentCommand{\procedure}{m m m m}{%
 \texttt{#1} & #2 & #3 & & #4 & & & & \\ \hline
}

%%%
%%% HEADER & FOOTER
%%%
% Increase header to account for lowered header
\geometry{top=3.6cm, headheight=3cm}
\makeatletter
% Generic header for portrait and landscape pages
\def\ac@head{%
	\ifweb
		\colorbox{ACheadercolor}{%
			\parbox[t][1.4cm][c]{\ac@headlen}{%
				\color{white}%
				\ac@head@content%
			}%
		}%
	\else
		% Header height chosen so that 1cm of header left between the top of the page and the header,
		% which is what most printers should be able to handle
		\parbox[t][3.8cm][c]{\ac@headlen}{%
				\normalcolor
				\ac@head@content
			}%
	\fi
}
% Content of the header, disregarding formatting and sizes
\def\ac@head@content{%
	\begin{center}%
		% Left side
		\parbox{5cm}{%
			\includegraphics[align=c,height=7.5mm]{media/logo}
			~
			\unless\iffinal%
			\emph{(Draft Version)}%
			\fi%
		}%
		\hspace{0.5cm}%
		% Center
		\parbox{\ac@headtitlelen}{%
			\centering%
			\theheadertitle
		}%
		\hspace{0.5cm}%
		% Right side
		\parbox{5cm}{\small\begin{flushright}%
				\mbox{\texttt{\thedpid}}%
		\end{flushright}}
	\end{center}%
}%
% Define the actual fancyhf header
\chead{%
	% Determine whether the page is in landscape or portrait mode
	\ifthenelse{\textwidth < \textheight}{%
		% Portrait
		\def\ac@headlen{\paperwidth}% The length of the header
		\def\ac@headtitlelen{6.5cm}% The length of the title box in the header
		% Absolute positioning of a \ac@headlen-wide box
		% in the top left corner (0,0) of the page
		\begin{textblock*}{\ac@headlen}(0cm,0cm)%
			\ac@head%
		\end{textblock*}%
	}{%
		% Landscape
		\def\ac@headlen{\paperheight}%
		\def\ac@headtitlelen{13.5cm}%
		\ifweb
			\def\ac@headtitleoffset{-1.1cm}
		\else
			\def\ac@headtitleoffset{-0.4cm}
		\fi
		% Since the header will be rotated, we have to set its height
		% here as the "length" of the block
		\begin{textblock*}{3.8cm}(\ac@headtitleoffset,0cm)%
			% Rotate the header. The lscape page only rotates the content of the page itself,
			% so an explicit rotation is needed for the header. This makes the page more
			% readable online and when the reader has opened it fully, but may not be
			% practical when shifting through a printed document.
			\rotatebox{90}{\ac@head}%
		\end{textblock*}%
	}%
}
\rhead{}
\rfoot{\thepage}

% Header/Footer style for lite pages (e.g. title page)
\fancypagestyle{plain}{%
	\fancyhf{}
	\renewcommand{\headrulewidth}{0pt}
}

