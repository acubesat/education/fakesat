\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cubedoc}[2022/01/10 AcubeSAT Documentation Class modified for FakeSat]

% Template version
\def\ACdoctemplateversion{vt1.8}

% Class options
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}

% Bibliography option
\makeatletter
\newif\if@loadbibliography\@loadbibliographyfalse
\DeclareOption{bibliography}{\@loadbibliographytrue}
\newif\iffinal\finalfalse
\DeclareOption{final}{\finaltrue}
\newif\ifpublic\publicfalse
\DeclareOption{public}{\publictrue}
\newif\ifdocidref\docidreffalse
\DeclareOption{docidref}{\docidreftrue}
\newif\ifletter\letterfalse
\DeclareOption{letter}{\lettertrue}
\newif\ifconfidential\confidentialfalse
\DeclareOption{confidential}{\confidentialtrue}
\newif\iflongtitles\longtitlesfalse
\DeclareOption{longtitles}{\longtitlestrue}
\newif\ifshowacronyms\showacronymstrue
\DeclareOption{hideacronyms}{\showacronymsfalse}
\newif\ifweb\webfalse
\DeclareOption{web}{\webtrue}
\def\ac@fontsize{11pt}
\DeclareOption{10pt}{\def\ac@fontsize{10pt}}
\DeclareOption{12pt}{\def\ac@fontsize{12pt}}
\makeatother
\ProcessOptions\relax

\IfFileExists{extra.local.tex}{\input{extra.local.tex}}{}

% Enable DOC-ID reference if the document is public
\ifpublic\docidreftrue\fi

\ifconfidential\ifpublic
\ClassError{cubedoc}{A document cannot be both confidential and public}{}
\stop
\fi\fi

\makeatletter
\LoadClass[a4paper,notitlepage,\ac@fontsize]{article}
\makeatother

% ==================
% ==   PACKAGES   ==
% ==================

% XMP data on final documents
\iffinal
\RequirePackage{hyperxmp}
\fi % \iffinal

% Support for colours
\RequirePackage[xcdraw,table]{xcolor}
\definecolor{asatblue}{HTML}{0B51C1} % Kept for backwards compatibility
\definecolor{spacedot}{HTML}{703200}
\definecolor{spacedotblue}{HTML}{2e98a3}
\definecolor{spacedotorange}{HTML}{ef6c00}
\definecolor{error}{rgb}{1,0.1,0.1}
\setlength\arrayrulewidth{0.6pt}

% Microtypography improvements
\iffinal
\RequirePackage{microtype}
\fi % \iffinal

% pdf links and link highlighting
\PassOptionsToPackage{hyphens}{url} % Don't try keeping URLs on one line
\RequirePackage{hyperref}

% Support for fancier tables
\RequirePackage{array}
\RequirePackage{multirow}
\RequirePackage{tabularx}
\RequirePackage{tablestyles}

% Link colours
\hypersetup{
	colorlinks,
	linkcolor={blue!60!black!90!green},
	citecolor={blue!50!black},
	urlcolor={blue!50!asatblue!95!black},
}

% Reduce list size
\RequirePackage[inline]{enumitem}
\setitemize{itemsep=1pt,topsep=0pt,parsep=0pt,partopsep=0pt}
\setenumerate{itemsep=1pt,topsep=0pt,parsep=0pt,partopsep=0pt}

% Reduced page margins
\RequirePackage[left=3cm,right=3cm,top=3cm,bottom=3cm,headheight=2cm]{geometry}

% Mathematics
\RequirePackage{mathtools}
\RequirePackage{amsmath}
%\RequirePackage{mathspec}
\RequirePackage{unicode-math}
%\setmathfont{TeX Gyre Heros Math}
%\setmainfont{XITS}
\setmathfont{TeX Gyre Schola Math}
%\setmathfont[range={\mathcal,\mathbfcal},StylisticSet=1]{XITS Math}
%\RequirePackage{tex-gyre-math}
%\setallmainfonts(Digits,Latin,Greek){Tex Gyre Termes Math}
%\setallmainfonts(Digits,Latin,Greek){media/texgyretermes-math.otf}

%Graphics
\RequirePackage{graphics}

% Acronyms
\IfFileExists{acro2.sty}{
	\ifweb
		\RequirePackage[hyperref=true]{acro2}
	\else
		\RequirePackage[hyperref=false]{acro2}
	\fi
}{
	\ifweb
		\RequirePackage[hyperref=true]{acro}
	\else
		\RequirePackage[hyperref=false]{acro}
	\fi
}

\RequirePackage{multicol}
\RequirePackage{ifthen}

% Count the number of acronyms and make it available as a variable
\makeatletter
\ExplSyntaxOn
\newcounter{ac@acronyms}
\AtBeginDocument{
	\acro_for_all_acronyms_do:n{ % For each acronym...
		%\acro_get:n {#1}%
		% If this acronym is to be printed
		\acro_if_entry:nnnT {#1} {} {}
		{
			% Increase the acronym counter
			\stepcounter{ac@acronyms}
		}
	}
}
\ExplSyntaxOff
\makeatother

% Language
\RequirePackage{polyglossia}
\RequirePackage{csquotes}
%\RequirePackage{xgreek}
\setdefaultlanguage{english}
\setotherlanguage{greek}

% Bibliography
\makeatletter
\if@loadbibliography
\RequirePackage[%
	sorting=none,%
	natbib=true,%
	backend=biber,%
	citestyle=authoryear,%
	style=numeric,%
	datamodel=applicabledoc,%
	defernumbers=true,%
	urldate=iso,%
	seconds=true,%
]{biblatex}
\RequirePackage{biblatex-ext-tabular}

% Workaround for biblatex-ext-tabular not working great with MD5 label prefixes
\patchcmd%
	\extblxtab@printbibtabular% % Patch the internal \printbibtabular command
	{{\blx@refcontext@labelprefix}}% % Find the label prefix in the \blx@xml@dlist@refcontext call
	{{\ifdef{\blx@refcontext@labelprefix@real}{\blx@refcontext@labelprefix@real}{\blx@refcontext@labelprefix}}}% % Replace it with the actual, non-MD5 label prefix (if this functionality has been implemented)
	{}{}

\fi
\makeatother

% Counter
\RequirePackage{totcount}

% Fonts
%\setmainfont{TeX Gyre Pagella}
%\newfontfamily\greekfont[Script=Greek]{TeX Gyre Pagella}
%\newfontfamily\greekfontsf[Script=Greek]{TeX Gyre Pagella}
%\newfontfamily\englishfont[Script=Latin]{TeX Gyre Pagella}
%\newfontfamily\englishfontsf[Script=Latin]{TeX Gyre Pagella}
\newfontfamily\englishfonttt[Script=Latin]{Latin Modern Mono}
\newfontfamily\displayfont[Script=Latin]{Noto Sans}

% Hyphenated monospace text
\DeclareTextFontCommand{\texthtt}{\ttfamily\hyphenchar\font=45\relax}

% Table magic and colours
\newcolumntype{x}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}p{\dimexpr#1-2\tabcolsep}}
\colorlet{almost}{green!70!blue!90!white}
\colorlet{tablegray}{gray!50!white}
\renewcommand{\arraystretch}{1.2}

% Custom headers and footers
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}
\RequirePackage{nameref}
\makeatletter
\newcommand*{\currentname}{\nouppercase{\leftmark}}
\makeatother
\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt} % Remove header line
\renewcommand{\sectionmark}[1]{\markboth{#1}{}}

% Define report header color
\iffinal
\colorlet{ACheadercolor}{spacedot}
\else
\colorlet{ACheadercolor}{red!50!black}
\fi

% Header/Footer style for regular pages
\makeatletter
\chead{%
	\vspace{-1cm}
	\hspace{-\oddsidemargin}\hspace{-1in}\hspace{-\hoffset}%
	%
	% Adjustbox makes a box that takes up the full width of the page
	\begin{adjustbox}{center,vspace*={{0in} {0.195cm}}}
		% The second \parbox parameter specifies the height of the header bar
		\colorbox{ACheadercolor}{\parbox[t][1.2cm][c]{21cm}{\begin{center}
					% Left side
					\parbox{4.5cm}{%
						\includegraphics[align=c,height=1cm]{media/logo}
						\unless\iffinal%
						\ \color{white}\emph{(Draft Version)}%
						\fi%
					}%
					\hspace{0.5cm}%
					% Center
					\parbox{7.5cm}{%
						\centering\color{white}%
						\theheadertitle
					}%
					\hspace{0.5cm}%
					% Right side
					\parbox{4.5cm}{\small\vspace{1pt}\begin{flushright}\color{white}%
							\unless\iflongtitles%
							\thedocid\\%
							\fi%
							\currentname%
					\end{flushright}}
		\end{center}}}
	\end{adjustbox}
}%
\rhead{}
\rfoot{\thepage}

% Special footer on confidential documents
\ifconfidential\lfoot{\small Confidential}\fi

% Header/Footer style for lite pages (e.g. title page)
\fancypagestyle{plain}{%
	\fancyhf{}
	\renewcommand{\headrulewidth}{0pt}
	\rfoot{\thepage}
	%\rfoot{\thepage/\pageref*{LastPage}}
}

% Header/Footer style for the changelog page
\fancypagestyle{changelog}{%
	\lfoot{} % Do not mention confidentiality, as it is explicitly written above

	\unless\ifletter
	\cfoot{\small\color{gray}Documentation template version \ACdoctemplateversion}
	\fi
}


\makeatother

% XeLaTeX character fixes
%\newsavebox{\myhbar}\savebox{\myhbar}{$\hbar$}
%\renewcommand*{\hbar}{\mathalpha{\usebox{\myhbar}}}

% TikZ and graphics
\RequirePackage{tikz}
\usetikzlibrary{decorations.pathreplacing}

% Macro commands
\newcommand{\h}{\hline}

% Spacing between paragraphs
\setlength{\parindent}{0pt}
\setlength{\parskip}{\baselineskip-5pt}%
\PolyglossiaSetup{english}{indentfirst=true}%
\PolyglossiaSetup{greek}{indentfirst=true}

% Floats labelling & captions
\renewcommand*\thetable{\Roman{table}} % Roman numbering for tables
\RequirePackage[format=plain,
labelfont={bf,it},
textfont=it]{caption}

% Consistent table styles
\setuptablestyle{default}{%
	\resettablestyle
	\renewcommand{\arraystretch}{1.4}
	\centering
	\upshape%
	\tablefontsize{body}
	\tablealtcolored%
	\setuptablecolor{head}{tablegray}
	\renewcommand{\thead}{%
		\tablefontsize{head}%
		\ignorespaces%
		\bfseries%
	}
	\renewcommand{\tsubhead}{%
		\tablefontsize{head}%
		\ignorespaces%
	}
}

\ExplSyntaxOn
% Workaround acro package hyperlinks not working in xetex
\cs_if_exist:NT \l__acro_use_hyperref_bool {
	\cs_set_protected:Npn \acro_activate_hyperref_support:
	{
		\bool_if:nT { \l__acro_hyperref_loaded_bool && \l__acro_use_hyperref_bool }
		{
			\sys_if_engine_xetex:TF
			{
				\cs_set:Npn \acro_hyper_link:nn ##1##2
				{ \hyperlink {##1} { \XeTeXLinkBox{##2} } }
			}
			{ \cs_set_eq:NN \acro_hyper_link:nn \hyperlink }
			\cs_set:Npn \acro_hyper_target:nn ##1##2
			{ \raisebox { 3ex } [ 0pt ] { \hypertarget {##1} { } } ##2 }
		}
	}
}

% Workaround acro showing errors 
\cs_if_exist:NF \g__acro_rerun_bool {
	\cs_new:Npn \g__acro_rerun_bool {\c_true_bool}
}

% Make acronym links uncolored, i.e. black, to blend in with the rest of the text
\cs_set:Npn \acro_color_link:n #1 {#1}

\ExplSyntaxOff

% Utility macros
\DeclareDocumentCommand{\docref}{ s o m }{%
	% Differentiate between starred and non-starred versions
	\IfBooleanTF {#1}%
	% Starred versions get just the text
	{\texttt{#3}}%
	% Non-starred calls get the link as well
	{\href{https://helit.org/mm/docList/#3}{\texttt{#3}}}%
	% Append the title of the report if provided
	\IfNoValueF{#2}{\ \textemdash\ #2}%
}

% Template for source code
\RequirePackage{listings}
\lstset{frame=tb,
	language=C++,
	aboveskip=3mm,
	belowskip=3mm,
	xleftmargin=\parindent,
	numbersep=5pt,
	showstringspaces=false,
	columns=flexible,
	keepspaces=true,
	basicstyle={\footnotesize\ttfamily},
	numbers=none,
	numberstyle=\tiny\color{gray},
	keywordstyle=\color{blue},
	commentstyle=\color{cyan},
	stringstyle=\color{magenta!50!blue},
	breaklines=true,
	breakatwhitespace=true,
	tabsize=3,
	backgroundcolor=\color{blue!30!gray!20!white}
}

% Custom environments
\RequirePackage{environ}

% Displays content only on confidential documents
\NewEnviron{confidential}{%
  \ifconfidential\BODY\else\relax\fi%
}

% Displays content only on public documents
\NewEnviron{public}{%
  \ifpublic\BODY\else\relax\fi%
}

% Custom authors command
\RequirePackage{etoolbox}
\RequirePackage{xparse}
\makeatletter
\newcommand{\ac@authortext}{}
\iffinal
\RequirePackage{ifmtarg}
\fi
\def\ac@authortext@comma{}%
\RenewDocumentCommand{\author}{om}{%
	\appto\ac@authortext{#2%
		\IfNoValueF{#1}{ (\href{mailto:#1}{\texttt{#1}})}
		\\}
	\iffinal
	% Append to the authortext variable that contains commas instead of newlines
	\ifdefempty{\ac@authortext@comma}{%
		\appto\ac@authortext@comma{#2}% Do not add a comma if this is the first element
	}{%
		\appto\ac@authortext@comma{, #2}%
	}
	\fi
}
\makeatother

\makeatletter
% Custom titlepage commands
\newcommand{\thedtitle}{}
\newcommand{\theheadertitle}{\thedtitle}
\LetLtxMacro\ac@old@title\title
\renewcommand{\title}[1]{\renewcommand{\thedtitle}{#1}\ac@old@title{#1}}
% Allow user to set a custom header title
\newcommand{\headertitle}[1]{\renewcommand{\theheadertitle}{#1}}
\makeatother

% Confidential watermark
\makeatletter
\ifconfidential
\@ifclassloaded{cubedp}{}{% Don't print watermark on cubedp
    \RequirePackage[printwatermark]{xwatermark}
    \newwatermark[pages=1,color=gray!30,angle=45,scale=2.5,xpos=-0.5cm,ypos=0]{\bf CONFIDENTIAL}
}
\fi
\makeatother
% Enable latex3 parsing
\ExplSyntaxOn
\newcommand{\thedocid}{}
\newcommand{\docid}[1]{
	% Store the doc id string
	\renewcommand{\thedocid}{#1}
	\iffinal
	% Define a sequence for the parts
	\seq_new:N \l_acubesat_id_args_seq
	% Split the string into parts, using - as a separator
	\seq_set_split:Nnn{\l_acubesat_id_args_seq}{-}{#1}
	% Define variables corresponding to each part
	\edef\docidss{\seq_item:Nn{\l_acubesat_id_args_seq}{2}}
	\edef\docidcat{\seq_item:Nn{\l_acubesat_id_args_seq}{3}}
	\edef\docidid{\seq_item:Nn{\l_acubesat_id_args_seq}{4}}
	\edef\docidtopcat{\str_range:Nnn{\docidcat}{1}{1}}
	\edef\docidsubcat{\str_range:Nnn{\docidcat}{2}{2}}
	% Run the command to find the categories
	%\l_acubesat_listcategories:
	\else
	\edef\docidss{}
	\edef\docidcat{}
	\edef\docidid{}
	\edef\docidtopcat{}
	\edef\docidsubcat{}
	\fi
}
\providecommand{\docidss}{}
\providecommand{\docidcat}{}
% Disable latex3 parsing
\ExplSyntaxOff

% Lists for the docid
\ExplSyntaxOn
\makeatletter
% Colouring & styling
\cs_new_protected:Npn \acubesat_colour_eq:nn #1 #2 {
	\str_if_eq:NNTF{#1}{#2}{\color{black}}{\color{ACinactive}}
}
\cs_new_protected:Npn \acubesat_style_id:n #1 {
	\textbf{\large\texttt{#1}}:
}
% List of subsystems
\prop_new:N \l_acubesat_subsystems_prop
\prop_put:Nnn \l_acubesat_subsystems_prop {GEN} {General}
\prop_put:Nnn \l_acubesat_subsystems_prop {ADC} {Attitude~Determination~\&~Control}
\prop_put:Nnn \l_acubesat_subsystems_prop {COM} {Communications}
\prop_put:Nnn \l_acubesat_subsystems_prop {EPS} {Electrical~Power}
\prop_put:Nnn \l_acubesat_subsystems_prop {OBC} {On-board~Data~Handling}
\prop_put:Nnn \l_acubesat_subsystems_prop {OPS} {Spacecraft~Operations}
\prop_put:Nnn \l_acubesat_subsystems_prop {SCI} {Science~Unit}
\prop_put:Nnn \l_acubesat_subsystems_prop {STR} {Structural}
\prop_put:Nnn \l_acubesat_subsystems_prop {SYE} {Systems~Engineering}
\prop_put:Nnn \l_acubesat_subsystems_prop {THE} {Thermal}
\prop_put:Nnn \l_acubesat_subsystems_prop {TRA} {Trajectory}
\prop_put:Nnn \l_acubesat_subsystems_prop {COL} {Collaboration}
\prop_put:Nnn \l_acubesat_subsystems_prop {PRG} {Programmatic}
% List of top categories
\prop_new:N \l_acubesat_categories_prop
\prop_put:Nnn \l_acubesat_categories_prop {E} {Experimentation~\&~Development}
\prop_put:Nnn \l_acubesat_categories_prop {B} {Research~\&~Technical~Background}
\prop_put:Nnn \l_acubesat_categories_prop {M} {Meeting~Outcomes}
\prop_put:Nnn \l_acubesat_categories_prop {T} {Technical~Specification}
\prop_put:Nnn \l_acubesat_categories_prop {G} {Technical~Guides~\&~Handbooks}
\prop_put:Nnn \l_acubesat_categories_prop {O} {Generic~\&~Operational}
% List of subcategories
% Experimentation & development
\prop_new:N \l_acubesat_categories_E_prop
\prop_put:Nnn \l_acubesat_categories_E_prop {C} {Components}
\prop_put:Nnn \l_acubesat_categories_E_prop {S} {Standards}
\prop_put:Nnn \l_acubesat_categories_E_prop {T} {Technologies}
\prop_put:Nnn \l_acubesat_categories_E_prop {W} {Software}
\prop_put:Nnn \l_acubesat_categories_E_prop {H} {Theoretical}
% Research & background
\prop_set_eq:NN \l_acubesat_categories_B_prop \l_acubesat_categories_E_prop
% Meeting outcomes
\prop_new:N \l_acubesat_categories_M_prop
\prop_put:Nnn \l_acubesat_categories_M_prop {I} {Internal}
\prop_put:Nnn \l_acubesat_categories_M_prop {X} {External}
% Technical specification
\prop_new:N \l_acubesat_categories_T_prop
\prop_put:Nnn \l_acubesat_categories_T_prop {G} {Generic}
\prop_put:Nnn \l_acubesat_categories_T_prop {U} {Subsystem~specific}
% Operational
\prop_new:N \l_acubesat_categories_O_prop
\prop_put:Nnn \l_acubesat_categories_O_prop {L} {Letters}
\prop_put:Nnn \l_acubesat_categories_O_prop {P} {Planning}
\prop_put:Nnn \l_acubesat_categories_O_prop {R} {Presentations}
\prop_put:Nnn \l_acubesat_categories_O_prop {A} {Policies~\&~Procedures}
% Functions that draw tables of the doc-id
% Subsystems table
\NewDocumentCommand\ACsubsystems{}{
	\begin{tabular}{r@{\hspace{0.5em}}l} % Table with reduced spacing
		% Loop over every subsystem
		\prop_map_inline:Nn \l_acubesat_subsystems_prop {%
			% Set colour based on equality of this subsystem to \docidss
			\acubesat_colour_eq:nn{##1}{\docidss}
			\acubesat_style_id:n{##1} % Subsystem ID
			&
			\acubesat_colour_eq:nn{##1}{\docidss}
			##2\\ % Subsystem name
		}
	\end{tabular}
}
% Categories table
\NewDocumentCommand\ACcategories{}{
	\begin{tabular}{r@{\hspace{0.5em}}l} % Table with reduced spacing
		% Loop over every subsystem
		\prop_map_inline:Nn \l_acubesat_categories_prop {%
			% Set colour based on equality of this category to \docidtopcat
			\acubesat_colour_eq:nn{##1}{\docidtopcat}
			\acubesat_style_id:n{##1} % Category ID
			&
			\acubesat_colour_eq:nn{##1}{\docidtopcat}
			##2\\ % Category name
			\bool_if:nTF{
				\str_if_eq_p:NN{##1}{\docidtopcat} && \prop_if_exist_p:c{l_acubesat_categories_##1_prop}}
			{
				% A list of sub-categories exists for this category; let's print them
				& \ACsubcategories{##1} \\
			}{}
		}
	\end{tabular}
}
% Subcategories table
\NewDocumentCommand\ACsubcategories{m}{
	\begin{tabular}{r@{\hspace{0.5em}}l}
		\prop_map_inline:cn{l_acubesat_categories_#1_prop}{%
			\acubesat_colour_eq:nn{##1}{\docidsubcat}
			\acubesat_style_id:n{##1} % Subcategory ID
			&
			\acubesat_colour_eq:nn{##1}{\docidsubcat}
			##2
			\\
		}
		\vspace{-3.1ex}
	\end{tabular}
}
% A list of all document categories
% Define a new global string for the full subsystem name
\str_new:N \g_acubesat_subsystem
% Define a new global string for the full category name
\str_new:N \g_acubesat_categories
% Define a new global sequence for all the categories
\seq_new:N \g_acubesat_categories_seq
% A command to calculate the categories
\cs_new:Npn \l_acubesat_listcategories: {
	% Take action based on whether the user provided a docID
	\ifdefempty {\thedocid} {
		% The user has not specified a docID, or specified an empty docID
		\ClassError{cubedoc}{Please~provide~a~valid~docID}{}
		% Store nothing to the variables of the subsystem and the categories
		\str_clear:N \g_acubesat_subsystem
		\str_clear:N \g_acubesat_categories
		% Set the docID to a value, so that latex doesn't complain for a lack of lines
		\renewcommand{\thedocid}{AcubeSAT-Unassigned}
	} {
		\prop_get:NVNF \l_acubesat_subsystems_prop {\docidss} \g_acubesat_subsystem {
			% The subsystem name was not found
			\ClassError{cubedoc}{Invalid~docID~subsystem~"\docidss"~provided}{}
			% Clear the string to prevent latex from hanging
			\str_clear:N \g_acubesat_subsystem
		}
		% Store the top category in a scratch string
		\prop_get:NVNTF \l_acubesat_categories_prop {\docidtopcat} \l_tmpa_str {
			% The top category was found, add it to the sequence
			\seq_put_right:Nx \g_acubesat_categories_seq \l_tmpa_str
		} {
			% The top category name was not found
			\ClassError{cubedoc}{Invalid~docID~category~"\docidtopcat"~provided}{}
			% Clear the string to prevent latex from hanging
			\str_clear:N \l_tmpa_str
		}
		% See if the subcategories array exists
		\prop_if_exist:cTF{l_acubesat_categories_\docidtopcat _prop}{
			% Store the subcategory in a scratch string
			\prop_get:cVNTF{l_acubesat_categories_\docidtopcat _prop}{\docidsubcat} \l_tmpb_str {
				% Add the category to the sequence
				\seq_put_right:Nx \g_acubesat_categories_seq \l_tmpb_str
				% The subcategory was found; store it in a scratch string
				\str_put_left:Nn \l_tmpb_str {,~}
			}{
				% The subcategory was not found; Destroy the scratch string
				\ClassError{cubedoc}{Invalid~docID~subcategory~"\docidsubcat"~provided}{}
				\str_clear:N \l_tmpb_str
			}

		}{
			% Make sure that the scratch string is clear if no subcategory exists
			\str_clear:N \l_tmpb_str
		}
		\tl_concat:NNN \g_acubesat_categories \l_tmpa_str \l_tmpb_str
	}
}
% Store as a command that can be used globally
\newcommand\ACsubsystem{
	\str_use:N \g_acubesat_subsystem
}
\newcommand\AClistcategories{
	\str_use:N \g_acubesat_categories
}
\makeatother
\ExplSyntaxOff

% Better appendix
\makeatletter
\newbool{ac@atappendix}
\let\ac@oldappendix\appendix
\renewcommand{\appendix}{%
	% Call the old appendix command
	\ac@oldappendix%
	% Set the internal appendix variable
	\booltrue{ac@atappendix}%
	% Add an Appendices entry to the table of contents
	\addtocontents{toc}{%
		\protect\contentsline{part}{\protect\numberline{}Appendices}{}{}%
	}%
}
\makeatother


\AtEndDocument{
	\ifbool{ac@atappendix}{}{\appendix}

	\iffinal\ifdocidref
	\newpage
	\section{Document-ID reference}

	\begin{center}
		\vfill
		\begin{tikzpicture}
		\newlength{\ACdashsep}
		\setlength{\ACdashsep}{2mm}
		\newlength{\ACversionsep}
		\setlength{\ACversionsep}{4cm}
		\newcommand{\ACidscale}{1.3}
		\definecolor{ACinactive}{rgb}{0.5,0.5,0.5}

		\begin{scope}[every node/.style={scale=\ACidscale}]
		\draw (0,0) node[text depth=0] (p1) {\texttt{AcubeSAT}};
		\draw (p1.east) ++(\ACdashsep,0) node[right] (p1d) {\texttt{\textemdash}};
		\draw (p1d.east) ++(\ACdashsep,0) node[right] (p2) {\texttt{\docidss}};
		\draw (p2.east) ++(\ACdashsep,0) node[right] (p2d) {\texttt{\textemdash}};
		\draw (p2d.east) ++(\ACdashsep,0) node[right] (p3) {\texttt{\docidcat}};
		\draw (p3.east) ++(\ACdashsep,0) node[right] (p3d) {\texttt{\textemdash}};
		\draw (p3d.east) ++(\ACdashsep,0) node[right] (p4) {\texttt{\docidid}};
		\draw (p4.east) ++(\ACversionsep,0) node (p5) {\texttt{\latestRev}};
		\end{scope}

		\draw [decorate,decoration={brace,amplitude=3pt,mirror},xshift=-4pt,yshift=0pt]
		(p2.south west) -- (p2.south east) node [midway] (p2brace) {};
		\draw [decorate,decoration={brace,amplitude=3pt,mirror},xshift=-4pt,yshift=0pt]
		(p3.south west) -- (p3.south east) node [midway] (p3brace) {};
		\draw [decorate,decoration={brace,amplitude=3pt,mirror},xshift=-4pt,yshift=0pt]
		(p4.south west) -- (p4.south east) node [midway] (p4brace) {};
		\draw [decorate,decoration={brace,amplitude=4pt,mirror},xshift=-4pt,yshift=0pt]
		(p5.south west) -- (p5.south east) node [midway] (p5brace) {};

		\draw[->] (p2brace) |- ++(1,-15) node[right] (lss) {Subsystem:};
		\draw (lss.east) node[right,align=left,xshift=0.0cm] {
			\ACsubsystems
		};


		\draw[->] (p3brace) |- ++(1,-7.5) node[right] (lcat) {Category:};
		\draw (lcat.east) node[right,align=left,xshift=-0.3cm] {
			\ACcategories
		};

		\draw[->] (p4brace) |- ++(1,-2.2) node[right] {Document number};
		\draw[->] (p5brace) |- ++(0.7,-0.8) node[right] {Version number};
		\end{tikzpicture}
		\vfill
		\begin{centering}
			\ifpublic
			For more information, see \url{https://helit.org/mm/docList/public/categories}
			\else
			For more information, see \url{https://helit.org/mm/docList/categories}
			\fi
		\end{centering}
	\end{center}
	\fi\fi % \ifdocidref, \iffinal
}

% Custom CubeSat title
\RequirePackage{adjustbox}
\RequirePackage{graphbox}
\makeatletter
\def\@maketitle{%
	\newpage%
	% Don't display fancy headers
	\thispagestyle{plain}
	% Define a new length for the separation of the cover
	\newlength{\coversep}%
	\setlength{\coversep}{\dimexpr(1.18in+\voffset+\topmargin+\headheight+\headsep+\parskip)\relax}%
	%
	% Adjustbox makes a box that takes up the full width of the page
	\begin{adjustbox}{center,vspace*={{-\coversep} {0in}}}
		% The second \parbox parameter specifies the height of the logo
		\colorbox{spacedot}{\parbox[b][5cm][c]{21cm}{\begin{center}
					\includegraphics[align=c,width=3.5cm]{media/logo}%
		\end{center}}}
	\end{adjustbox}

	\null
	\vskip 8em%
	\begin{center}%
		\let \footnote \thanks%
		{\Huge\textbf\@title}\\[2ex]%
		{\LARGE\texttt{\thedocid}}\\[2ex]%
		{\large\ac@authortext}%
	\end{center}%
	\par
	\vskip 5em%
	\begin{center}%
		{\LARGE\today}\\%
		{\Large Version: \texttt{\latestRev}}%
		\unless\iffinal%
		\\{\color{error}\Large Draft version}
		\fi%
		\ifconfidential%
		\\[3ex]{\color{spacedot!50!error}\Large Confidential Document}%
		\fi%
	\end{center}%
	\par
	\vfill%
	\begin{center}%
		FakeSat\\[3ex]%
		\the\year%
	\end{center}%
	\clearpage\newpage%
}
\makeatother

% For table of contents manipulation
\RequirePackage{tocloft}
\setlength{\cftbeforesecskip}{3pt} % Reduce the space between TOC entries

% A list of acceptable changelog entries
\ExplSyntaxOn
\seq_set_from_clist:Nn \l_acubesat_changelog_statuses_seq {
	DRAFT,
	INTERNALLY~RELEASED,
	PUBLISHED,
	REVOKED
}
\seq_new:N \l_acubesat_changelog_xml_seq
% Changelog commands
\newcommand{\thechangelog}{}
\newcommand{\latestRev}{0.0}
\newcommand{\changelog}[4]{%
	% Prepend the version to the changelog table
	\preto\thechangelog{\hline#1 & \texttt{#2} & #3 & #4\\}%
	% Set the latest revision variable
	\renewcommand{\latestRev}{#2}
	% Show an error if the version is invalid
	\seq_if_in:NnTF \l_acubesat_changelog_statuses_seq {#3} {} {\ClassError{cubedoc}{Invalid~changelog~Document~Status.~Provide~one~of:~
			DRAFT,~INTERNALLY~RELEASED,~PUBLISHED,~REVOKED
		}{}}
	% Add the version to the XML list
	\iffinal
	\prop_clear:N \l_tmpa_prop % Empty the temporary prop list to use it
	% Add the properties to the property list
	\prop_put:Nnn \l_tmpa_prop {modifyDate} {#1}
	\prop_put:Nnn \l_tmpa_prop {status} {#3}
	\prop_put:Nnn \l_tmpa_prop {version} {#2}
	% Prepend the property list to the sequence of versions
	\seq_put_right:NV \l_acubesat_changelog_xml_seq {\l_tmpa_prop}
	\fi
}
\ExplSyntaxOff

% Set keeppdfinfo, so that legacy clients that do not support XMP still show the metadata
% \hypersetup has to be executed here, before the production of the PDF starts
\iffinal
\hypersetup{keeppdfinfo}
\fi

% Things to do when beginning the document
\makeatletter
% Define the preamble command (allow it to be overriden)
\def\ac@preamble{
	% Hypersetup has to be executed here, so that all other variables are ready
	\hypersetup{%
		pdfauthor   = {\ac@authortext@comma},
		pdfkeywords = {\thedocid, \AClistcategories},
		pdftitle    = {\@title},
		pdfsubject  = {\@title},
	}

	\maketitle

	\tableofcontents

	\unless\ifletter
	\section*{Changelog}\vspace{-4ex}
	\bgroup % Begin group to keep length parameters local
	\thispagestyle{changelog}
	\setlist{nosep,after=\vspace{-\baselineskip}}
	\renewcommand{\arraystretch}{1.5}
	\begin{adjustbox}{center}
		\begin{tabular}{|c|c|>{\centering\arraybackslash}p{2.5cm}|p{7.9cm}|}
			\hline
			\rowcolor{gray!50!white}
			\textbf{Date} & \textbf{Version} & \parbox{2.7cm}{\vspace{3pt}\centering\textbf{Document\\Status}\vspace{3pt}} & \multicolumn{1}{c|}{\textbf{Comments}} \\
			\thechangelog
			\hline
		\end{tabular}
	\end{adjustbox}
	\egroup
	\fi

	\iffinal
	This is the latest version of this document (\texttt{\latestRev}) as of \today.
	\unless\ifletter
	\ifpublic
	Newer versions might be available at \url{https://helit.org/mm/docList/public/\thedocid}.
	\else
	Newer versions might be available at \url{https://helit.org/mm/docList/\thedocid}.
	\fi
	\fi
	\vspace{1cm}

	\ifpublic\unless\ifletter
	\par

	\iffinal % Add a signature to the final document
	\begin{flushright} % Align the signature to the right
		\hbox{ % A box containing the signature. Will be visible to latex as well.
			\special{pdf:ann width 3in height 1in % PDF annotation for the signature
				<<                                  % (only for compatible readers)
				/T (Publisher Signature)
				/Subtype /Widget
				/FT /Sig
				/F 4
				/Q 1
				/MK << /BC [] >>
				>>
			}%
			\rule{0mm}{1in}\kern3in % Make the box large enough to hold the signature.
		}
	\end{flushright}
	\fi
	\fi\fi
	\else
	This is a {\color{error}draft} version (\texttt{\color{error}\latestRev}) of this document, as of \today.
	\fi

	\ifconfidential
	This document contains confidential data, sensitive information, or assets covered by Non-Disclosure Agreements. As such, it should be considered confidential and sharing must be restricted.
	\fi

	\ifshowacronyms%
		\ifthenelse{\theac@acronyms>0}{%
			\section*{Acronyms}
			\begin{multicols}{2}%
				\printacronyms[heading=none]
			\end{multicols}%
		}{}
	\fi

	\ifdef{\afterpreamble}{\afterpreamble}{%
		\clearpage%
		\newpage%
	}
}
\AfterEndPreamble{
	\ac@preamble
}
\makeatother

% Unimplemented cubedp.cls commands to allow compatibility of documents
\newcommand{\dpid}[1]{}
\newcommand{\distribution}[1]{}

